package org.gcube.contentmanagement.blobstorage.transport.plugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.gcube.contentmanagement.blobstorage.resource.MemoryType;
import org.gcube.contentmanagement.blobstorage.resource.RequestObject;
import org.gcube.contentmanagement.blobstorage.resource.StorageObject;
import org.gcube.contentmanagement.blobstorage.service.operation.Copy;
import org.gcube.contentmanagement.blobstorage.service.operation.CopyDir;
import org.gcube.contentmanagement.blobstorage.service.operation.Download;
import org.gcube.contentmanagement.blobstorage.service.operation.DuplicateFile;
import org.gcube.contentmanagement.blobstorage.service.operation.Link;
import org.gcube.contentmanagement.blobstorage.service.operation.Lock;
import org.gcube.contentmanagement.blobstorage.service.operation.Move;
import org.gcube.contentmanagement.blobstorage.service.operation.MoveDir;
import org.gcube.contentmanagement.blobstorage.service.operation.Unlock;
import org.gcube.contentmanagement.blobstorage.service.operation.Upload;
import org.gcube.contentmanagement.blobstorage.transport.TransportManager;

import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanagement.blobstorage.transport.plugin.Utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import com.mongodb.MongoException;

public class S3PluginManager extends TransportManager {


	S3Client s3;
	private Logger logger = LoggerFactory.getLogger(S3PluginManager.class);
	private Region region;
	private String token;
	
	

	@Override
	public String getName() {
		return "S3";
	}
	
	public S3PluginManager() {
		logger.info("S3");
		
		
	}
	
	public S3PluginManager(String[] server, String user, String pass, MemoryType memoryType, String[] dbNames,
			String writeConcern, String readConcern, String token, String region) {
		initBackend(server,user,pass, memoryType,dbNames, writeConcern, readConcern, token, region);
	}
	
	
	@Override
	public void initBackend(String[] server, String accessKey, String secretAccessKey, MemoryType memoryType, String[] dbNames,
			String writeConcern, String readConcern, String token, String region) {
		this.memoryType=memoryType;
		AwsSessionCredentials awsCreds =null;
		if(!Objects.isNull(getToken()))
			awsCreds = AwsSessionCredentials.create(accessKey, secretAccessKey, token);
		else 
			awsCreds = AwsSessionCredentials.create(accessKey, secretAccessKey, "");
		this.region = Region.of(region);
		s3 = S3Client.builder().credentialsProvider(
	             StaticCredentialsProvider.create(awsCreds))
	.endpointOverride(URI.create(server[0])).region(this.region).build();
	}
		

	@Override
	public Object get(Download download) throws FileNotFoundException, IOException {
		String bucketName=Utils.convertToS3Format(download.getResource().getRootPath());
		return BucketOperator.getInstance(s3).getObject(bucketName, Utils.convertToS3Format(download.getResource().getRemotePath()), download.getLocalPath());
	}

	@Override
	public String put(Upload upload) throws FileNotFoundException, IOException {
		logger.trace("put method invoked ");
		String bucketName=Utils.convertToS3Format(upload.getResource().getRootPath());
		if (!BucketOperator.isBucket(s3, bucketName))
			BucketOperator.createBucketWaiter(s3, bucketName, region);
		File file= new File(upload.getResource().getLocalPath());
		return BucketOperator.getInstance(s3).putObject(bucketName, Utils.convertToS3Format(upload.getResource().getRemotePath()), file, upload.isReplaceOption());
	}

	@Override
	public Map<String, StorageObject> getValues(RequestObject resource, String bucket, Class<? extends Object> type) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public void removeRemoteFile(String bucket, RequestObject resource) throws UnknownHostException {
		 BucketOperator.getInstance(s3).deleteObject(Utils.convertToS3Format(resource.getRootPath()), Utils.convertToS3Format(resource.getRemotePath()));

	}

	@Override
	public void removeDir(String remoteDir, RequestObject myFile) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");

	}

	@Override
	public long getSize(String key, RequestObject file) {
		logger.info("remoteObject identifier "+key);
		try {
			return BucketOperator.getObjectSize(Utils.convertToS3Format(file.getRootPath()), Utils.convertToS3Format(file.getRemotePath()));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RemoteBackendException("Problem during getSize Operation on s3 cloud storage");
		}
	}

	@Override
	public String lock(Lock lock) throws Exception {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String unlock(Unlock unlock) throws FileNotFoundException, UnknownHostException, MongoException, Exception {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public long getTTL(String pathServer) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public long renewTTL(RequestObject resource) throws UnknownHostException, IllegalAccessException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String link(Link link) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String copy(Copy copy) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String move(Move move) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public List<String> copyDir(CopyDir copy) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public List<String> moveDir(MoveDir move) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String getFileProperty(String remotePath, String property) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public long getFolderTotalItems(String folderPath) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public long getFolderTotalVolume(String folderPath) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String getUserTotalVolume(String user) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String getUserTotalItems(String user) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public boolean isValidId(String id) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String getId(String remoteIdentifier, boolean forceCreation) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String getField(String remoteIdentifier, String fieldName) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public void close() {
		s3.close();

	}
	
	@Override
	public void forceClose() {
		s3.close();

	}

	@Override
	public void setFileProperty(String remotePath, String propertyField, String propertyValue) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");

	}

	@Override
	public String getRemotePath(String bucket) throws UnknownHostException {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public boolean exist(String bucket) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}

	@Override
	public String duplicateFile(DuplicateFile duplicate) {
		throw new RemoteBackendException("method not implemented yet on s3 plugin");
	}
	
	protected String getToken() {
		return token;
	}

	protected void setToken(String token) {
		this.token = token;
	}




}

