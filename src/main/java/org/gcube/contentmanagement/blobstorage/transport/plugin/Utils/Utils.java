package org.gcube.contentmanagement.blobstorage.transport.plugin.Utils;

import org.gcube.contentmanagement.blobstorage.transport.backend.util.Costants;

public class Utils {
	
	public static String convertToS3Format(String objectName) {		
		if( objectName.substring(objectName.length()-1).contains(Costants.FILE_SEPARATOR)) {
			objectName=objectName.substring(0, objectName.length()-1);
		}
		if( objectName.substring(0).contains(Costants.FILE_SEPARATOR)) {
			objectName=objectName.substring(1, objectName.length());
		}
		objectName=objectName.toLowerCase();
		return objectName.replaceAll(Costants.FILE_SEPARATOR, "-");
	}

}
