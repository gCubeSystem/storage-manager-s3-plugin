# Changelog for storage-manager-s3-plugin

## [v1.0.0-SNAPSHOT] 2021-11-08
  * update storage-manager-core range
  * clean code

## [v0.0.1-SNAPSHOT] 2020-11-06
  * first release
  