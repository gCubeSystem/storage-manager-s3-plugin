storage-manger-s3-plugin
----

[![experimental](http://badges.github.io/stability-badges/dist/experimental.svg)](http://github.com/badges/stability-badges)

storage-manager-s3-plugin is a Java plugin of storage-manager-core library. It enables to perform CRUD operation on  any S3 compliant object storage. The object-storage credentials are retrieved automatically from a gCube ServiceEndpoint with coordinates DataStorage - StorageManager.
    
## Examples of use


## Deployment
    
Notes about how to deploy this component on an infrastructure or link to wiki doc (if any).

## Documentation
See storage-manager-core on [Wiki](https://gcube.wiki.gcube-system.org/gcube/Storage_Manager).

Some notes:

 * only the CRUD operations were implemented in this version;
 * this component is a storage-manager-core plugin so it works only with the storage-manager libraries (wrapper and core): it could be loaded at runtime or it could be included in the pom as a static dependency;
 * the API are the same used for MongoDB but the new storage-manager-wrapper version (starting from 3.0.0-SNAPSHOT) contains a new constructor that permit to specify the S3 Backend as input parameter;
 * for the moment there are two different object storage that can be automatically discovered and used: "Zadara" is discovered automatically on NextNext; "VaiSulWeb" is discovered automatically on devVRE;
 * the S3 object storage is discovered automatically only if the S3 Backend is passed as input parameter;
 * if the VOLATILE memory is specified as input parameter, the storage-manager will use MongoDB Volatile area even if the S3 backend is specified as input parameter;
This component can be used with storage-manager-wrapper and core starting from 3.0.0-SNAPSHOT version;



## License
TBP